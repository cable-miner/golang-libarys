all: gobox
# GOPATH := $(shell pwd)

PATH := $(PWD)/bin:$(PATH)
OUTPUT_DIR=build
DIR=$(shell pwd)

deps: libs 
		gx --verbose install --global
# 			make -C src/gobox godeps
		
#libs:  $(GOPATH)/src/github.com/ipfs/go-ipfs $(GOPATH)/src/bitbucket.org/cable-miner/gobox
	
		
#$(GOPATH)/src/github.com/ipfs/go-ipfs:
#		go get -d github.com/ipfs/go-ipfs && cd $(GOPATH)/src/github.com/ipfs/go-ipfs && gx --verbose install --global

		
$(PWD)/src/gobox:
		git clone https://cable-miner@bitbucket.org/cable-miner/gobox.git $(PWD)/src/gobox
		
#$(GOPATH)/src/bitbucket.org/cable-miner/gobox: $(PWD)/src/gobox#
#		mkdir -p $(GOPATH)/src/bitbucket.org/cable-miner
#		ln -sf $(PWD)/src/gobox $(GOPATH)/src/bitbucket.org/cable-miner/gobox
		

		
clean:
		rm -fR hotpatch pkg build gobox src/github.com */gx/ipfs vendor/src/gx bin/tmp
		
webshell.o: webshell.go
		go build -o webshell.o webshell.go 
		#gccgo -c $< -o $@ -fgo-prefix=webshell

		
if-godeps-faild:
		go get github.com/BurntSushi/toml
		go get github.com/armon/consul-api
		go get github.com/ccding/go-stun
		go get github.com/codegangsta/cli
		go get github.com/dchest/uniuri
		go get github.com/docopt/docopt-go
		go get github.com/flynn/go-shlex
		go get github.com/fsnotify/fsnotify
		go get github.com/golang/protobuf/proto
		go get github.com/golang/protobuf/ptypes/any
		go get github.com/google/go-github/github
		go get github.com/google/go-querystring/query
		go get github.com/gorilla/context
		go get github.com/gorilla/mux
		go get github.com/hashicorp/hcl
		go get github.com/ipfs/go-ipfs
		go get github.com/jbenet/go-base58
		go get github.com/kardianos/osext
		go get github.com/kr/fs
		go get github.com/kr/pty
		go get github.com/magiconair/properties
		go get github.com/mitchellh/mapstructure
		go get github.com/onsi/ginkgo
		go get github.com/onsi/gomega
		go get github.com/pkg/errors
		go get github.com/pkg/sftp
		go get github.com/spf13/cast
		go get github.com/spf13/jwalterweatherman
		go get github.com/spf13/pflag
		go get github.com/spf13/viper
		go get github.com/stretchr/testify/assert
		go get github.com/stretchr/testify/vendor/github.com/davecgh/go-spew/spew
		go get github.com/stretchr/testify/vendor/github.com/pmezard/go-difflib/difflib
		go get github.com/surma/gobox/pkg/common
		go get github.com/tomsteele/cookiescan
		go get github.com/ugorji/go/codec
		go get github.com/urfave/cli
		go get github.com/xordataexchange/crypt/backend
		go get github.com/xordataexchange/crypt/config
		go get github.com/xordataexchange/crypt/encoding/secconf
		go get golang.org/x/crypto/cast5
		go get golang.org/x/crypto/curve25519
		go get golang.org/x/crypto/ed25519
		go get golang.org/x/crypto/openpgp
		go get golang.org/x/crypto/ripemd160
		go get golang.org/x/crypto/ssh
		go get golang.org/x/sys/unix
		go get github.com/mkideal/cli
		go get github.com/badoux/goscraper
		go get github.com/fatih/color
		go get github.com/hashicorp/go-version
		go get github.com/xrash/smetrics
		go get github.com/ti/nasync
		go get github.com/unixist/postex/discovery
		go get github.com/drael/GOnetstat
		go get github.com/surma/gobox/pkg/common
		go get github.com/ipfs/go-ipfs/repo/config
		go get github.com/ipfs/go-ipfs/core/corenet
		go get 
		# go build pathBrute.go  


godeps: bin/gvt
		gvt restore
		make -C src/gobox godeps


		
cross_compile:  
# 		xgo -ldflags='-linkmode external -extldflags "-static"' -tags=release -pkg cmd/urlscan -buildmode=exe -branch="master" -targets="linux/amd64" bitbucket.org/cable-miner/golang-libarys
		xgo  -tags=release -pkg cmd/urlscan -buildmode=exe -branch="master" -targets="linux/amd64" bitbucket.org/cable-miner/golang-libarys

	#export GOPATH=/usr/local/go
	#	export PATH="/usr/local/gonative/bin:/usr/local/go/bin:$(PATH):$(GOPATH)/bin" 
#		gox -os="linux" -arch="386 amd64" -output "$(OUTPUT_DIR)/pkg/{{.OS}}_{{.Arch}}/{{.Dir}}" gobox
#export PATH="$(PWD)/src:$(PWD)/bin/:$(PWD)/src/gobox/bin:$(PATH)" 
# 		ln -sf $(PWD)/src/gobox $(GOPATH)/src/bitbucket.org/cable-miner/gobox
		#cd src/gobox && make  cross_compile  
		
# TARGET_OS="linux/386,linux/amd64,linux/arm-5"

		
# build/urlscan:	
# 		xgo -ldflags='-linkmode external -extldflags "-static"' -tags=release -pkg cmd/urlscan -buildmode=exe -branch="master" -targets=$(TARGET_OS) bitbucket.org/cable-miner/golang-libarys		
# 	
MODULES=pnscan urlscan libipfs
TARGET_OS="linux/amd64"	
FILES      = $(wildcard cmd/*)

all: $(MODULES)


build/ipfshost:
		go build -o build/ipfs  cmd/ipfscmd/ipfscmd.go 
		
build/disco:
		go build -o build/disco cmd/disco/disco.go 

		
src/utmp:
		ln -sf "$(DIR)/$(BASE)/utmp" "$(GOPATH)/src/utmp"
src/disco: 
		BASE=$(shell basename $@)
# 		ln -sf "$(DIR)/$(BASE)/disco" "$(DIR)/src/disco"
		ln -sf "$(DIR)/$(BASE)/disco" "$(GOPATH)/src/disco"				

src/libipfs: 
		BASE=$(shell basename $@)
# 		ln -sf "$(DIR)/$(BASE)/libipfs" "$(DIR)/$@"
		ln -sf "$(DIR)/$(BASE)/libipfs" "$(GOPATH)/$@"
		
src/urlscan: 
		BASE=$(shell basename $@)
# 		ln -sf "$(DIR)/$(BASE)" "$(DIR)/$@"
		ln -sf "$(DIR)/$(BASE)" "$(GOPATH)/$@"
		
build/$(FILES): 
		BASE=$(shell basename $@)
		xgo -ldflags='-linkmode external -extldflags "-static"' -tags=release -pkg "cmd/$@" -buildmode=exe -branch="master" -targets=$(TARGET_OS) bitbucket.org/cable-miner/golang-libarys	
	

links: $(MODULES)
	@echo TEMP $<
	
	
tools:	bin bin/dist_get bin/gx bin/gx-go bin/gx bin/gox bin/gvt 
	#gb build github.com/ipfs/go-ipfs/cmd/...
	#gb build ./cmd/..

	
bin:	
		mkdir bin
bin/gvt:
		go get -u github.com/FiloSottile/gvt   
		
bin/dist_get:
		curl https://raw.githubusercontent.com/ipfs/go-ipfs/master/bin/dist_get > bin/dist_get
		chmod +x bin/dist_get
bin/gb:
		go get github.com/constabulary/gb/...
		
bin/gx:
		dist_get /ipfs/QmXZQzBAFuoELw3NtjQZHkWSdA332PyQUj6pQjuhEukvg8 gx $(PWD)/bin/gx v0.7.0
		
bin/gx-go:
		dist_get /ipfs/QmXZQzBAFuoELw3NtjQZHkWSdA332PyQUj6pQjuhEukvg8 gx-go $(PWD)/bin/gx-go v1.2.0
		
bin/gox:
		go get github.com/mitchellh/gox
		
bin/ipfshost:   bin/gb
		gb build cmd/ipfshost
		
#ipfs-tools: $(GOPATH)/src/github.com/ipfs/go-ipfs
#		gb build github.com/ipfs/go-ipfs/cmd/...
		
targz:
		mkdir -p ${OUTPUT_DIR}/dist
		cd ${OUTPUT_DIR}/pkg/; for osarch in *; do (cd $$osarch; tar zcvf ../../dist/gotty_$$osarch.tar.gz ./*); done;
