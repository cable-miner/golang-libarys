// +build  !arm
package cookiescan

///*
// #cgo CFLAGS: -Ilib/libpcap -Isrc/vendor/libpcap-libpcap-1.7.4/pcap/ -Isrc/vendor/libpcap-libpcap-1.7.4/ -I../../src/vendor/libpcap-libpcap-1.7.4/pcap/ -I../../src/pcap/pcap
// #cgo LDFLAGS:  -Llib/libpcap/libpcap.a
//*/
import "C"

import (
	// 	"flag"
	//"bitbucket.org/cable-miner/golang-libarys/pnscan"

	"bufio"
	"errors"
	"fmt"
	"log"
	"net"
	"os"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/cable-miner/golang-libarys/getrange"
	//"bitbucket.org/cable-miner/golang-libarys/urlscan"
	"github.com/miekg/pcap"

	"github.com/dbiesecke/cookiescan"
	"github.com/docopt/docopt-go"
)

// var (
// // 	flagSet  = flag.NewFlagSet("cookiescan", flag.PanicOnError)
// // 	helpFlag = flagSet.Bool("help", false, "Show this help")
// // 	versionFlag = flagSet.Bool("version", false, "version")
// // 	interfaceFlag = flagSet.String("interface","","Network interface to listen on.")
// 	options = parse()
// 	filter  = "tcp[13] == 0x11 or tcp[13] == 0x10 or tcp[13] == 0x18"
// )

func Cookiescan(call []string) error {

	// 	e := flagSet.Parse(call[1:])
	// 	if e != nil {
	// 		return e
	// 	}

	test := make([]string, 4)
	test[0] = "urlscan"
	test[1] = "-verbose"
	test[2] = "-stdout"
	//myrange :=
	//myrange :=
	//go urlscan.Urlscan(test)
	test[0] = "cookiescan"
	test[1] = "-p"
	test[2] = "80"
	test[3] = getrange.GetRange()

	main(getrange.GetRange())
	// 	if flagSet.NArg() != 1 || *helpFlag {
	// 		println("`cookiescan` [options] <192.168.1.1/24>")
	// 		flagSet.PrintDefaults()
	// 		return nil
	// 	}
	return nil
}

type empty struct{}
type task struct {
	ip   string
	port int
}

func main(target string) {
	var (
		options = parse(target)
		filter  = "tcp[13] == 0x11 or tcp[13] == 0x10 or tcp[13] == 0x18"
	)

	h, err := pcap.OpenLive(options.device, int32(320), true, 500)
	if err != nil {
		log.Fatal(err.Error())
	}
	if err = h.SetFilter(filter); err != nil {
		log.Fatal(err.Error())
	}
	db := cookiescan.NewStore(options.ips)

	var (
		track = make(chan empty)
		tasks = make(chan task, options.minconcurrency)
	)
	go func() {
		for pkt, r := h.NextEx(); r >= 0; pkt, r = h.NextEx() {
			select {
			case <-track:
				break
			default:
				if r == 0 {
					continue
				}
				pkt.Decode()
				if len(pkt.Headers) < 2 {
					continue
				}
				iphdr, ok := pkt.Headers[0].(*pcap.Iphdr)
				if !ok {
					continue
				}
				if len(iphdr.SrcIp) < 4 {
					continue
				}
				ip := fmt.Sprintf("%v.%v.%v.%v", iphdr.SrcIp[0], iphdr.SrcIp[1], iphdr.SrcIp[2], iphdr.SrcIp[3])
				tcphdr, ok := pkt.Headers[1].(*pcap.Tcphdr)
				if !ok {
					continue
				}
				db.Add(ip, int(tcphdr.SrcPort), tcphdr.FlagsString())
			}
		}
		h.Close()
	}()

	for i := 0; i < options.minconcurrency; i++ {
		go func() {
			for tsk := range tasks {
				c, err := net.DialTimeout("tcp", fmt.Sprintf("%s:%d", tsk.ip, tsk.port), options.timeout)
				if err != nil {
					continue
				}
				c.Close()
			}
		}()
	}

	//uiprogress.Start()
	//bar := uiprogress.AddBar(len(options.ips) * len(options.services))
	//bar.AppendCompleted()
	//bar.PrependElapsed()

	for _, ip := range options.ips {
		for _, p := range options.services {
			tasks <- task{ip, p}
			//bar.Incr()
		}
	}

	close(tasks)
	time.Sleep(time.Duration(2 * time.Second))
	track <- empty{}
	close(track)
	//uiprogress.Stop()
	db.JSON(options.minconfidence, "test.json")

	db.Tabbed(options.minconfidence)
}

////////////

const usage = `

Usage:
  cookiescan [options] <target>
  cookiescan -h | --help
  cookiescan -v | --version

Required Arguments:
  target:           IP Address, Hostname, or CIDR network. May also be a a newline separated
                    file containing targets.

Options:
  -h --help         Show this message.
  -v --version      Show version.
  -p <port ranges>  Ports to scan. Ex: -p 22; -p 1-65535, -p 80,443. [default: 1-1024]
  -e <port ranges>  Ports to exclude from scan. Ex: -e 22; -p 21,23. [default: 0]
  -g <int>          Amount of goroutines to spread connection attempts across. [default: 500]
  -c <int>          Minimum confidence level to flag port as open. [default: 1]
  -i <interface>    Network interface to listen on.
  -t <timeout>      Timeout in Milliseconds to wait for a connection. [default: 400]
  -j <file>         Output JSON to file.
  -r 		        randome mode


`

type O struct {
	services       []int
	minconfidence  int
	minconcurrency int
	timeout        time.Duration
	device         string
	ips            []string
	jsonfile       string
}

func parse(target string) *O {
	args, err := docopt.Parse(usage, nil, true, "cookiescan 2.1.0", false)
	if err != nil {
		log.Fatalf("Error parsing usage. Error: %s\n", err.Error())
	}
	if err != nil {
		log.Fatalf("Error parsing usage. Error: %s\n", err.Error())
	}
	o := &O{}

	if jsonfile, ok := args["-j"].(string); ok {
		o.jsonfile = jsonfile
	}

	var lines []string
	if target == "" {
		hostorfile := args["<target>"].(string)
		if ok, err := os.Stat(hostorfile); err == nil && ok != nil {
			if lines, err = readFileLines(hostorfile); err != nil {
				log.Fatalf("Error parsing input file. Error: %s\n", err.Error())
			}
		} else {
			lines = append(lines, hostorfile)
		}
	} else {
		hostorfile := target
		if ok, err := os.Stat(hostorfile); err == nil && ok != nil {
			if lines, err = readFileLines(hostorfile); err != nil {
				log.Fatalf("Error parsing input file. Error: %s\n", err.Error())
			}
		} else {
			lines = append(lines, hostorfile)
		}
	}

	if o.ips, err = linesToIPList(lines); err != nil {
		log.Fatalf("Error parsing targets. Error: %s\n", err.Error())
	}

	if o.services, err = explode(args["-p"].(string)); err != nil {
		log.Fatalf("Error parsing port string. Error %s\n", err.Error())
	}
	servicesToExclude, err := explode(args["-e"].(string))
	if err != nil && args["-e"].(string) != "0" {
		log.Fatalf("Error parsing exclude port string. Error %s\n", err.Error())
	}
	for _, e := range servicesToExclude {
		for i, s := range o.services {
			if e == s {
				if len(o.services) <= 1 {
					log.Fatal("Error parsing exlude port range. Resulting port list leaves no ports to scan")
				}
				o.services = append(o.services[:i], o.services[i+1:]...)
			}
		}
	}

	if o.minconfidence, err = strconv.Atoi(args["-c"].(string)); err != nil {
		log.Fatal("Invalid argument for -c.")
	}
	if o.minconcurrency, err = strconv.Atoi(args["-g"].(string)); err != nil {
		log.Fatal("Invalid argument for -g.")
	}

	ti, err := strconv.Atoi(args["-t"].(string))
	if err != nil {
		log.Fatal("Invalid argument for -t.")
	}
	o.timeout = time.Duration(ti) * time.Millisecond

	if args["-i"] != nil {
		o.device = args["-i"].(string)
	}
	if o.device == "" {
		devs, err := pcap.FindAllDevs()
		if err != nil {
			log.Fatal("Error finding interfaces. Error: ", err)
		}
		if len(devs) == 0 {
			log.Fatal("No interfaces found. Are you not running as root?")
		}
		o.device = devs[0].Name
	}

	return o
}

// linesToIPList processes a list of IP addresses or networks in CIDR format.
// Returning a list of all possible IP addresses.
func linesToIPList(lines []string) ([]string, error) {
	ipList := []string{}
	for _, line := range lines {
		if net.ParseIP(line) != nil {
			ipList = append(ipList, line)
		} else if ip, network, err := net.ParseCIDR(line); err == nil {
			for ip := ip.Mask(network.Mask); network.Contains(ip); increaseIP(ip) {
				ipList = append(ipList, ip.String())
			}
		} else {
			return ipList, fmt.Errorf("%s is not an IP Address or CIDR Network", line)
			ips, err := net.LookupIP(line)
			if err != nil {
				return ipList, fmt.Errorf("%s is not a valid hostname", line)
			}
			ipList = append(ipList, ips[0].String())
		}
	}
	return ipList, nil
}

// increases an IP by a single address.
func increaseIP(ip net.IP) {
	for j := len(ip) - 1; j >= 0; j-- {
		ip[j]++
		if ip[j] > 0 {
			break
		}
	}
}

// readFileLines returns all the lines in a file.
func readFileLines(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	lines := []string{}
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		if scanner.Text() == "" {
			continue
		}
		lines = append(lines, scanner.Text())
	}
	return lines, scanner.Err()
}

// Turns a string of ports separated by '-' or ',' and returns a slice of Ints.
func explode(s string) ([]int, error) {
	const errmsg = "Invalid port specification"
	ports := []int{}
	switch {
	case strings.Contains(s, "-"):
		sp := strings.Split(s, "-")
		if len(sp) != 2 {
			return ports, errors.New(errmsg)
		}
		start, err := strconv.Atoi(sp[0])
		if err != nil {
			return ports, errors.New(errmsg)
		}
		end, err := strconv.Atoi(sp[1])
		if err != nil {
			return ports, errors.New(errmsg)
		}
		if start > end || start < 1 || end > 65535 {
			return ports, errors.New(errmsg)
		}
		for ; start <= end; start++ {
			ports = append(ports, start)
		}
	case strings.Contains(s, ","):
		sp := strings.Split(s, ",")
		for _, p := range sp {
			i, err := strconv.Atoi(p)
			if err != nil {
				return ports, errors.New(errmsg)
			}
			if i < 1 || i > 65535 {
				return ports, errors.New(errmsg)
			}
			ports = append(ports, i)
		}
	default:
		i, err := strconv.Atoi(s)
		if err != nil {
			return ports, errors.New(errmsg)
		}
		if i < 1 || i > 65535 {
			return ports, errors.New(errmsg)
		}
		ports = append(ports, i)
	}
	return ports, nil
}
