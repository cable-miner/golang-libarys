package libipfs

import "flag"

import (
	"bufio"
	"fmt"
	"github.com/surma/gobox/pkg/common"
	"io"
	"os"
	"os/exec"
	"strings"
	"time"

	core "github.com/ipfs/go-ipfs/core"
	corenet "github.com/ipfs/go-ipfs/core/corenet"
	config "github.com/ipfs/go-ipfs/repo/config"
	fsrepo "github.com/ipfs/go-ipfs/repo/fsrepo"
	peer "gx/ipfs/QmQGwpJy9P4yXZySmqkZEXCmbBpJUb8xntCv8Ca4taZwDC/go-libp2p-peer"

	"gx/ipfs/QmZy2y8t9zQH2a1b8q2ZSLKp17ATuJoCNxxyMFG5qFExpt/go-net/context"
)

var (
	flagSet   = flag.NewFlagSet("shell2ipfs", flag.PanicOnError)
	helpFlag  = flagSet.Bool("help", false, "Show this help")
	stdinFlag = flagSet.Bool("stdin", false, "stdin")
	repoFlag  = flagSet.String("repo", "~/.ipfs", "repo path")
	idFlag  =  flagSet.String("id", "QmYKdVY4cjVHfoPbRr26eooMgs8oU1ETB3Gbx9GDmTM9SK", "id")
)

var con *common.BufferedReader

func Shell2ipfs(call []string) error {

	e := flagSet.Parse(call[1:])
	if e != nil {

		return e
	}
	target, err := peer.IDB58Decode(*idFlag)
	if ( flagSet.NArg() == 1 )  {
		*idFlag = flagSet.Arg(0)
		println("`shell2ipfs-client` [options] <parameter1>")
		flagSet.PrintDefaults()
		//return nil
		target, _ = peer.IDB58Decode(flagSet.Arg(0))
		
	}
	//fmt.Fprintf(os.Stderr, "Connect %s\n", target )
	


	// Basic ipfsnode setup
	r, err := fsrepo.Open(*repoFlag)
	if err != nil {
		// panic(err)
		daemonLocked, err := fsrepo.LockedByOtherProcess(*repoFlag)
		if err != nil {
			return err
		}

		if daemonLocked {
			fmt.Fprintf(os.Stderr, "Daemon allready running %s\n", repoFlag)
			return nil
		}

		var conf *config.Config

		fsrepo.Init(*repoFlag, conf)
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	cfg := &core.BuildCfg{
		Repo:   r,
		Online: true,
	}

	nd, err := core.NewNode(ctx, cfg)

	if err != nil {
		panic(err)
	}

	fmt.Printf("I am peer %s dialing %s\n", nd.Identity, target)

	con, err := corenet.Dial(nd, target, "/app/whyrusleeping")
	if err != nil {
		fmt.Println(err)
		return err
	}
	go io.Copy(os.Stdout, con)
	//go io.Copy(os.Stdin, con)

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		fmt.Fprintln(con, scanner.Text())
	}

	//	go io.Copy( os.Stdout, con )

	if *stdinFlag == true {
		c := make(chan error, 1)
		go readIt()
		select {
		case err := <-c:
			// use err and reply
			fmt.Fprintf(os.Stderr, "ERRor %s\n", err)
		case <-time.After(1 * time.Minute):
			//fmt.Fprintf(os.Stderr, "force \n", os.Args[0])

		}

	}

	//io.Copy(os.Stdout, con)

	return nil

}

func readIt() {
	reader := bufio.NewScanner(os.Stdout)
	for reader.Scan() {
		fmt.Fprintln(os.Stderr, reader.Text())
	}
	return
}

func isComment(line string) bool {
	line = strings.TrimSpace(line)
	return strings.HasPrefix(line, "#")
}

func execute(cmd []string) ([]byte, error) {
	if len(cmd) == 0 {
	}
	cmdl, err := exec.Command(cmd[0], cmd[1:]...).Output()
	if err != nil {
		//log.Fatal(err)
	}
	return cmdl, nil

}

func shell() {
	var in *common.BufferedReader
	interactive := false
	in = common.NewBufferedReader(os.Stdin)

	var e error
	var line string
	var output []byte
	for e == nil {
		if interactive {
			fmt.Print("> ")
		}
		line, e = in.ReadWholeLine()
		if e != nil {

		}
		if isComment(line) {
			continue
		}
		params, ce := common.Parameterize(line)
		if ce != nil {
			common.DumpError(ce)
			continue
		}

		output, ce = execute(params)
		if ce != nil {
			common.DumpError(ce)
			continue
		}
		fmt.Printf(string(output))
	}
	return

}
