
Easy Golang Update with godeb
================================
* On debian based systems: [godep](https://godeb.s3.amazonaws.com/godeb-amd64.tar.gz)


```shell

$ xgo -pkg cmd/ipfshost -branch="master" -targets="linux/amd64" -image nated/go -depsargs="--disable-bluetooth" -deps="https://github.com/the-tcpdump-group/libpcap/archive/libpcap-1.7.4.tar.gz" bitbucket.org/cable-miner/gobox



```



Build static release
====================================


```shell

$ xgo -ldflags='-linkmode external -extldflags "-static"' -tags=release -pkg cmd/pnscan -buildmode=exe  -branch="master" -targets="linux/amd64" bitbucket.org/cable-miner/golang-libarys
Checking docker installation...
