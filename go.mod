module bitbucket.org/cable-miner/golang-libarys

go 1.17

require (
	github.com/dbiesecke/cookiescan v2.4.1+incompatible
	github.com/docopt/docopt-go v0.0.0-20180111231733-ee0de3bc6815
	github.com/miekg/pcap v1.0.1
	github.com/tellytv/go.xtream-codes v0.0.0-20190427212115-45e8162ba888
	gopkg.in/resty.v1 v1.12.0
)

require (
	github.com/cretz/bine v0.2.0 // indirect
	github.com/danwakefield/fnmatch v0.0.0-20160403171240-cbb64ac3d964 // indirect
	github.com/gokeen/keen v1.2.0 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/helloyi/go-sshclient v1.1.0 // indirect
	github.com/ilius/go-askpass v0.0.0-20200508084844-f833acf6c50c // indirect
	github.com/ipsn/go-libtor v1.0.380 // indirect
	github.com/jaytaylor/html2text v0.0.0-20200412013138-3577fbdbcff7 // indirect
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/mxschmitt/playwright-go v0.1400.0 // indirect
	github.com/natedat/shutils v0.0.0-20181124074802-a15b7edd9c3a // indirect
	github.com/olekukonko/tablewriter v0.0.5 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/savaki/jq v0.0.0-20161209013833-0e6baecebbf8 // indirect
	github.com/ssor/bom v0.0.0-20170718123548-6386211fdfcf // indirect
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519 // indirect
	golang.org/x/net v0.0.0-20211005001312-d4b1ae081e3b // indirect
	golang.org/x/sys v0.0.0-20211002104244-808efd93c36d // indirect
	golang.org/x/term v0.0.0-20201126162022-7de9c90e9dd1 // indirect
	gopkg.in/dutchcoders/goftp.v1 v1.0.0-20170301105846-ed59a591ce14 // indirect
	gopkg.in/h2non/gentleman.v2 v2.0.5 // indirect
	gopkg.in/httgo/interfaces.v1 v1.0.0 // indirect
	gopkg.in/nowk/jsonify.v1 v1.0.0 // indirect
	gopkg.in/square/go-jose.v2 v2.6.0 // indirect
)
